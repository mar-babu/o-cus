-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2019 at 06:45 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbelearning`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblautonumbers`
--

CREATE TABLE `tblautonumbers` (
  `AUTOID` int(11) NOT NULL,
  `AUTOSTART` varchar(30) NOT NULL,
  `AUTOEND` int(11) NOT NULL,
  `AUTOINC` int(11) NOT NULL,
  `AUTOKEY` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblexercise`
--

CREATE TABLE `tblexercise` (
  `ExerciseID` int(11) NOT NULL,
  `LessonID` int(11) NOT NULL,
  `Question` text NOT NULL,
  `ChoiceA` text NOT NULL,
  `ChoiceB` text NOT NULL,
  `ChoiceC` text NOT NULL,
  `ChoiceD` text NOT NULL,
  `Answer` varchar(90) NOT NULL,
  `ExercisesDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblexercise`
--

INSERT INTO `tblexercise` (`ExerciseID`, `LessonID`, `Question`, `ChoiceA`, `ChoiceB`, `ChoiceC`, `ChoiceD`, `Answer`, `ExercisesDate`) VALUES
(20180001, 6, 'What is the title of the video', 'My Father', 'My Mother', 'My Brother', 'My Sister', 'My Sister', '0000-00-00'),
(20180002, 6, 'Who is the name of the character in the story?', 'Ben', 'Holly', 'Gaston', 'Wise old elf', 'Gaston', '0000-00-00'),
(20190003, 10, '1. What is right way to Initialize array?', 'int num[6] = { 2, 4, 12, 5, 45, 5 };', 'int n{} = { 2, 4, 12, 5, 45, 5 };', 'int n{6} = { 2, 4, 12 };', 'int n(6) = { 2, 4, 12, 5, 45, 5 };', 'int num[6] = { 2, 4, 12, 5, 45, 5 };', '0000-00-00'),
(20190004, 10, '2. An array elements are always stored in ________ memory locations.', 'Sequential.', 'Random.', 'Sequential and Random.', 'None of the above.', 'Sequential.', '0000-00-00'),
(20190005, 10, '3. What is the maximum number of dimensions an array in C may have?', '2', '8', 'Theoretically no limit.', '20', 'Theoretically no limit.', '0000-00-00'),
(20190006, 10, '4. Size of the array need not be specified, when', 'Initialization is a part of definition.', 'It is a declaratrion.', 'It is a formal parameter.', 'All of these.', 'All of these.', '0000-00-00'),
(20190007, 10, '5. Array passed as an argument to a function is interpreted as', 'Address of the array.', 'Values of the first elements of the array.', 'Address of the first element of the array.', 'Number of element of the array.', 'Address of the first element of the array.', '0000-00-00'),
(20190008, 12, '1.  Which one of the following regular expression matches any string containing zero or one p?', 'p+', 'p*', 'p#', 'P?', 'P?', '0000-00-00'),
(20190009, 12, '2.  [:alpha:] can also be specified as ________', '[A-Za-z0-9]', '[A-za-z]', '[A-z]', '[a-z]', '[A-za-z]', '0000-00-00'),
(201900010, 12, ' 3. POSIX implementation was deprecated in which version of PHP?', 'PHP 4', 'PHP 5', 'PHP 5.2', 'PHP 5.3', 'PHP 5.3', '0000-00-00'),
(201900011, 12, '4. POSIX stands for ____________', 'Portable Operating System Interface for Linux', 'Portable Operating System Interface for Unix', 'Portative Operating System Interface for Unix', 'Portative Operating System Interface for Linux', 'Portable Operating System Interface for Unix', '0000-00-00'),
(201900012, 12, '5. String is basically ?', 'Data Set', 'Null Set', 'Offset', 'Carracter set', 'Carracter set', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tbllesson`
--

CREATE TABLE `tbllesson` (
  `LessonID` int(11) NOT NULL,
  `LessonChapter` varchar(90) NOT NULL,
  `LessonTitle` varchar(90) NOT NULL,
  `FileLocation` text NOT NULL,
  `Category` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbllesson`
--

INSERT INTO `tbllesson` (`LessonID`, `LessonChapter`, `LessonTitle`, `FileLocation`, `Category`) VALUES
(10, 'C Programming', 'Introduction of Array in C', 'files/Arrays in C.mp4', 'Video'),
(11, 'JavaScript', 'Getting Started', 'files/1. Section Intro.mp4', 'Video'),
(12, 'PHP', 'à¦ªà¦¿à¦à¦‡à¦šà¦ªà¦¿à¦¤à§‡ à¦¬à¦¿à¦­à¦¿à¦¨à§à¦¨à¦­à¦¾à¦¬à§‡ à¦¸à§à¦Ÿà§à¦°à¦¿à¦‚ à¦²à§‡', 'files/à§¬.à§§ - à¦ªà¦¿à¦à¦‡à¦šà¦ªà¦¿à¦¤à§‡ à¦¬à¦¿à¦­à¦¿à¦¨à§à¦¨à¦­à¦¾à¦¬à§‡ à¦¸à§à¦Ÿà§à¦°à¦¿à¦‚ à¦²à§‡à¦–à¦¾à¦° à¦ªà¦¦à§à¦§à¦¤à¦¿.MP4', 'Video'),
(13, 'Object Oriented Programming (CPP)', 'Balagurusami-Object-Oriented-Programming-CPP', 'files/balagurusami-objectoriented-programming-cPP-NoRestriction.pdf', 'Docs');

-- --------------------------------------------------------

--
-- Table structure for table `tblscore`
--

CREATE TABLE `tblscore` (
  `ScoreID` int(11) NOT NULL,
  `LessonID` int(11) NOT NULL,
  `ExerciseID` int(11) NOT NULL,
  `StudentID` int(11) NOT NULL,
  `NoItems` int(11) NOT NULL DEFAULT 1,
  `Score` int(11) NOT NULL,
  `Submitted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblscore`
--

INSERT INTO `tblscore` (`ScoreID`, `LessonID`, `ExerciseID`, `StudentID`, `NoItems`, `Score`, `Submitted`) VALUES
(34, 10, 20190003, 1, 1, 0, 1),
(35, 10, 20190004, 1, 1, 0, 1),
(36, 10, 20190005, 1, 1, 1, 1),
(37, 10, 20190006, 1, 1, 0, 1),
(38, 10, 20190007, 1, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblstudent`
--

CREATE TABLE `tblstudent` (
  `StudentID` int(11) NOT NULL,
  `Fname` varchar(90) NOT NULL,
  `Lname` varchar(90) NOT NULL,
  `Address` varchar(90) NOT NULL,
  `MobileNo` varchar(90) NOT NULL,
  `STUDUSERNAME` varchar(90) NOT NULL,
  `STUDPASS` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblstudent`
--

INSERT INTO `tblstudent` (`StudentID`, `Fname`, `Lname`, `Address`, `MobileNo`, `STUDUSERNAME`, `STUDPASS`) VALUES
(1, 'Abdur', 'Rahman', 'Mirpur 02', '01681195152', 'ar', '7c4a8d09ca3762af61e59520943dc26494f8941b'),
(2, 'a', 'b', 'mirpur', '03254133', 'ab', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220'),
(13, 'a', 'aaa', 'sdsds', '26562', 'io', '7c4a8d09ca3762af61e59520943dc26494f8941b');

-- --------------------------------------------------------

--
-- Table structure for table `tblstudentquestion`
--

CREATE TABLE `tblstudentquestion` (
  `SQID` int(11) NOT NULL,
  `ExerciseID` int(11) NOT NULL,
  `LessonID` int(11) NOT NULL,
  `StudentID` int(11) NOT NULL,
  `Question` varchar(90) NOT NULL,
  `CA` varchar(90) NOT NULL,
  `CB` varchar(90) NOT NULL,
  `CC` varchar(90) NOT NULL,
  `CD` varchar(90) NOT NULL,
  `QA` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblstudentquestion`
--

INSERT INTO `tblstudentquestion` (`SQID`, `ExerciseID`, `LessonID`, `StudentID`, `Question`, `CA`, `CB`, `CC`, `CD`, `QA`) VALUES
(3, 20190003, 0, 9, 'What will be the output of the following C code?\r\n\r\n    #include <stdio.h>\r\n    void main(', 'Same address is printed.', 'Different address is printed.', 'Compile time error.', 'Nothing.', 'Same address is printed.'),
(4, 20190003, 0, 10, 'What will be the output of the following C code?\r\n\r\n    #include <stdio.h>\r\n    void main(', 'Same address is printed.', 'Different address is printed.', 'Compile time error.', 'Nothing.', 'Same address is printed.'),
(5, 20190003, 0, 11, 'What will be the output of the following C code?\r\n\r\n    #include <stdio.h>\r\n    void main(', 'Same address is printed.', 'Different address is printed.', 'Compile time error.', 'Nothing.', 'Same address is printed.'),
(6, 20190003, 0, 12, 'What will be the output of the following C code?\r\n\r\n    #include <stdio.h>\r\n    void main(', 'Same address is printed.', 'Different address is printed.', 'Compile time error.', 'Nothing.', 'Same address is printed.'),
(7, 20190004, 0, 9, 'An array elements are always stored in ________ memory locations.', 'Sequential.', 'Random.', 'Sequential and Random.', 'None of the above.', 'Sequential.'),
(8, 20190004, 0, 10, 'An array elements are always stored in ________ memory locations.', 'Sequential.', 'Random.', 'Sequential and Random.', 'None of the above.', 'Sequential.'),
(9, 20190004, 0, 11, 'An array elements are always stored in ________ memory locations.', 'Sequential.', 'Random.', 'Sequential and Random.', 'None of the above.', 'Sequential.'),
(10, 20190004, 0, 12, 'An array elements are always stored in ________ memory locations.', 'Sequential.', 'Random.', 'Sequential and Random.', 'None of the above.', 'Sequential.'),
(11, 20190005, 0, 9, 'What is the maximum number of dimensions an array in C may have?', '2', '8', 'Theoratically no limit. The only practical limits are memory size and compilers.', '20', 'Theoratically no limit. The only practical limits are memory size and compilers.'),
(12, 20190005, 0, 10, 'What is the maximum number of dimensions an array in C may have?', '2', '8', 'Theoratically no limit. The only practical limits are memory size and compilers.', '20', 'Theoratically no limit. The only practical limits are memory size and compilers.'),
(13, 20190005, 0, 11, 'What is the maximum number of dimensions an array in C may have?', '2', '8', 'Theoratically no limit. The only practical limits are memory size and compilers.', '20', 'Theoratically no limit. The only practical limits are memory size and compilers.'),
(14, 20190005, 0, 12, 'What is the maximum number of dimensions an array in C may have?', '2', '8', 'Theoratically no limit. The only practical limits are memory size and compilers.', '20', 'Theoratically no limit. The only practical limits are memory size and compilers.'),
(15, 20190006, 0, 9, 'Size of the array need not be specified, when', 'Initialization is a part of definition.', 'It is a declaratrion.', 'It is a formal parameter.', 'All of these.', 'All of these.'),
(16, 20190006, 0, 10, 'Size of the array need not be specified, when', 'Initialization is a part of definition.', 'It is a declaratrion.', 'It is a formal parameter.', 'All of these.', 'All of these.'),
(17, 20190006, 0, 11, 'Size of the array need not be specified, when', 'Initialization is a part of definition.', 'It is a declaratrion.', 'It is a formal parameter.', 'All of these.', 'All of these.'),
(18, 20190006, 0, 12, 'Size of the array need not be specified, when', 'Initialization is a part of definition.', 'It is a declaratrion.', 'It is a formal parameter.', 'All of these.', 'All of these.'),
(19, 20190007, 0, 9, 'Array passed as an argument to a function is interpreted as', 'Address of the array.', 'Values of the first elements of the array.', 'Address of the first element of the array.', 'Number of element of the array.', 'Address of the first element of the array.'),
(20, 20190007, 0, 10, 'Array passed as an argument to a function is interpreted as', 'Address of the array.', 'Values of the first elements of the array.', 'Address of the first element of the array.', 'Number of element of the array.', 'Address of the first element of the array.'),
(21, 20190007, 0, 11, 'Array passed as an argument to a function is interpreted as', 'Address of the array.', 'Values of the first elements of the array.', 'Address of the first element of the array.', 'Number of element of the array.', 'Address of the first element of the array.'),
(22, 20190007, 0, 12, 'Array passed as an argument to a function is interpreted as', 'Address of the array.', 'Values of the first elements of the array.', 'Address of the first element of the array.', 'Number of element of the array.', 'Address of the first element of the array.'),
(23, 20190008, 0, 9, '1.  Which one of the following regular expression matches any string containing zero or on', 'p+', 'p*', 'p#', 'P?', 'P?'),
(24, 20190008, 0, 10, '1.  Which one of the following regular expression matches any string containing zero or on', 'p+', 'p*', 'p#', 'P?', 'P?'),
(25, 20190008, 0, 11, '1.  Which one of the following regular expression matches any string containing zero or on', 'p+', 'p*', 'p#', 'P?', 'P?'),
(26, 20190008, 0, 12, '1.  Which one of the following regular expression matches any string containing zero or on', 'p+', 'p*', 'p#', 'P?', 'P?'),
(27, 20190009, 0, 9, '2.  [:alpha:] can also be specified as ________', '[A-Za-z0-9]', '[A-za-z]', '[A-z]', '[a-z]', '[A-za-z]'),
(28, 20190009, 0, 10, '2.  [:alpha:] can also be specified as ________', '[A-Za-z0-9]', '[A-za-z]', '[A-z]', '[a-z]', '[A-za-z]'),
(29, 20190009, 0, 11, '2.  [:alpha:] can also be specified as ________', '[A-Za-z0-9]', '[A-za-z]', '[A-z]', '[a-z]', '[A-za-z]'),
(30, 20190009, 0, 12, '2.  [:alpha:] can also be specified as ________', '[A-Za-z0-9]', '[A-za-z]', '[A-z]', '[a-z]', '[A-za-z]'),
(31, 201900010, 0, 9, ' 3. POSIX implementation was deprecated in which version of PHP?', 'PHP 4', 'PHP 5', 'PHP 5.2', 'PHP 5.3', 'PHP 5.3'),
(32, 201900010, 0, 10, ' 3. POSIX implementation was deprecated in which version of PHP?', 'PHP 4', 'PHP 5', 'PHP 5.2', 'PHP 5.3', 'PHP 5.3'),
(33, 201900010, 0, 11, ' 3. POSIX implementation was deprecated in which version of PHP?', 'PHP 4', 'PHP 5', 'PHP 5.2', 'PHP 5.3', 'PHP 5.3'),
(34, 201900010, 0, 12, ' 3. POSIX implementation was deprecated in which version of PHP?', 'PHP 4', 'PHP 5', 'PHP 5.2', 'PHP 5.3', 'PHP 5.3'),
(35, 201900011, 0, 9, '4. POSIX stands for ____________', 'Portable Operating System Interface for Linux', 'Portable Operating System Interface for Unix', 'Portative Operating System Interface for Unix', 'Portative Operating System Interface for Linux', 'Portable Operating System Interface for Unix'),
(36, 201900011, 0, 10, '4. POSIX stands for ____________', 'Portable Operating System Interface for Linux', 'Portable Operating System Interface for Unix', 'Portative Operating System Interface for Unix', 'Portative Operating System Interface for Linux', 'Portable Operating System Interface for Unix'),
(37, 201900011, 0, 11, '4. POSIX stands for ____________', 'Portable Operating System Interface for Linux', 'Portable Operating System Interface for Unix', 'Portative Operating System Interface for Unix', 'Portative Operating System Interface for Linux', 'Portable Operating System Interface for Unix'),
(38, 201900011, 0, 12, '4. POSIX stands for ____________', 'Portable Operating System Interface for Linux', 'Portable Operating System Interface for Unix', 'Portative Operating System Interface for Unix', 'Portative Operating System Interface for Linux', 'Portable Operating System Interface for Unix'),
(39, 201900012, 0, 9, '5. String is basically ?', 'Data Set', 'Null Set', 'Offset', 'Carracter set', 'Carracter set'),
(40, 201900012, 0, 10, '5. String is basically ?', 'Data Set', 'Null Set', 'Offset', 'Carracter set', 'Carracter set'),
(41, 201900012, 0, 11, '5. String is basically ?', 'Data Set', 'Null Set', 'Offset', 'Carracter set', 'Carracter set'),
(42, 201900012, 0, 12, '5. String is basically ?', 'Data Set', 'Null Set', 'Offset', 'Carracter set', 'Carracter set');

-- --------------------------------------------------------

--
-- Table structure for table `tblusers`
--

CREATE TABLE `tblusers` (
  `USERID` int(11) NOT NULL,
  `NAME` varchar(90) NOT NULL,
  `UEMAIL` varchar(90) NOT NULL,
  `PASS` varchar(90) NOT NULL,
  `TYPE` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblusers`
--

INSERT INTO `tblusers` (`USERID`, `NAME`, `UEMAIL`, `PASS`, `TYPE`) VALUES
(1, 'Md. Abdur Rahman', 'abdur', '2e1fcad8461c55339cc5f30e26da386c1e22c9bf', 'Administrator');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblautonumbers`
--
ALTER TABLE `tblautonumbers`
  ADD PRIMARY KEY (`AUTOID`);

--
-- Indexes for table `tblexercise`
--
ALTER TABLE `tblexercise`
  ADD PRIMARY KEY (`ExerciseID`);

--
-- Indexes for table `tbllesson`
--
ALTER TABLE `tbllesson`
  ADD PRIMARY KEY (`LessonID`);

--
-- Indexes for table `tblscore`
--
ALTER TABLE `tblscore`
  ADD PRIMARY KEY (`ScoreID`);

--
-- Indexes for table `tblstudent`
--
ALTER TABLE `tblstudent`
  ADD PRIMARY KEY (`StudentID`) USING BTREE;

--
-- Indexes for table `tblstudentquestion`
--
ALTER TABLE `tblstudentquestion`
  ADD PRIMARY KEY (`SQID`);

--
-- Indexes for table `tblusers`
--
ALTER TABLE `tblusers`
  ADD PRIMARY KEY (`USERID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblautonumbers`
--
ALTER TABLE `tblautonumbers`
  MODIFY `AUTOID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tblexercise`
--
ALTER TABLE `tblexercise`
  MODIFY `ExerciseID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201900013;

--
-- AUTO_INCREMENT for table `tbllesson`
--
ALTER TABLE `tbllesson`
  MODIFY `LessonID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tblscore`
--
ALTER TABLE `tblscore`
  MODIFY `ScoreID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `tblstudent`
--
ALTER TABLE `tblstudent`
  MODIFY `StudentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tblstudentquestion`
--
ALTER TABLE `tblstudentquestion`
  MODIFY `SQID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `tblusers`
--
ALTER TABLE `tblusers`
  MODIFY `USERID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
